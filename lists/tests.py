from django.http import HttpResponse
from django.http.request import HttpRequest
from django.template.loader import render_to_string
from django.test import TestCase
from django.core.urlresolvers import resolve
from lists.views import home_page


class HomePageTest(TestCase):
    def test_root_url_resolves_to_home_page_view(self):
        found = resolve('/')
        self.assertEqual(found.func, home_page)

    def test_home_page_returns_correct_html(self):
        request = HttpRequest()
        response = home_page(request)

        expected_html = render_to_string('home.html')

        self.assertEqual(response.content.decode(), expected_html)

    def test_home_page_can_save_a_post_request(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['item_text'] = 'A New Item.'

        response = home_page(request)

        self.assertIn('A New Item.', response.content.decode())
        expected_html = render_to_string(
            'home.html',
            {'new_text_item': 'A New Item.'}
        )
        self.assertEqual(response.content.decode(), expected_html)

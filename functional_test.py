from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import unittest


class NewVisitor(unittest.TestCase):
    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def test_can_start_a_list_and_retrive_it_later(self):
        # علی درباره یک برنامه TO DO مطالبی شنیده است.
        # او می خواهد سری به وبسایت برنامه بزند.
        self.browser.get('http://localhost:8000')

        # او انتظار دارد در عنوان وبسایت کلمه TO-DO را ببیند.
        self.assertIn('TO-DO', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('TO DO', header_text)

        self.assertEqual(
            input_box.get_attribute('placeholder'),
            'Enter a to-do item.'
        )

        input_box = self.browser.find_element_by_id('id_new_item')
        input_box.send_keys('some buy.')
        input_box.send_keys(Keys.ENTER)

        input_box = self.browser.find_element_by_id('id_new_item')
        input_box.send_keys('second item.')
        input_box.send_keys(Keys.ENTER)

        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('tr')

        self.assertIn('1: some buy.', [row.text for row in rows])

        self.assertIn('2: second item.', [row.text for row in rows])

        self.fail('finish the test.')


if __name__ == "__main__":
    unittest.main(warnings='ignore')
